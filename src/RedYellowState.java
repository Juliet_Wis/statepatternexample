public class RedYellowState implements State{

    @Override
    public String getColor() {
        return "GREEN";
    }

    @Override
    public State getState() {
        return new GreenState();
    }
}
