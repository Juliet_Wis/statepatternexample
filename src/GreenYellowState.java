public class GreenYellowState implements State {
    @Override
    public String getColor() {
        return "RED";
    }

    @Override
    public State getState() {
        return new RedState();
    }
}
