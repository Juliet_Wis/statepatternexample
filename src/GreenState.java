public class GreenState implements State {
    @Override
    public String getColor() {
        return "YELLOW";
    }

    @Override
    public State getState() {
        return new GreenYellowState();
    }
}
