public class TrafficLight {
    private State state = new GreenState();

    public void setState(State state){
        this.state = state;
    }

    public String getColor(){
        state = state.getState();
        return state.getColor();
    }
}
