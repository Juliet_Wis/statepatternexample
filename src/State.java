public interface State {
    String getColor();
    State getState();
}
